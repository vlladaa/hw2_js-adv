//Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
//її доречно використовувати, наприклад, коли маєш справу з банківською системою та фінансовими операціями
//або ж при взаємодії з користувачем, щоб перевірити коректність введених даних

const root = document.querySelector('#root')
const ul = document.createElement('ul')
root.appendChild(ul)

const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

books.forEach(function (book) {
    try {
      if (book.name && book.author && book.price) {
        let li = document.createElement('li');
       li.innerHTML = `автор: ${book.author};<br> назва: "${book.name}";<br> ціна: ${book.price}.`;
        ul.appendChild(li);
      } else {
        if (!book.name) {
          throw new Error('Відсутня властивість "name": ' + JSON.stringify(book));
        }
        if (!book.author) {
          throw new Error('Відсутня властивість "author": ' + JSON.stringify(book));
        }
        if (!book.price) {
          throw new Error('Відсутня властивість "price": ' + JSON.stringify(book));
        }
      }
    } catch (er) {
        console.error(er);
    }
});